public class SimpleWar {
	public static void main(String[] args) {
		//initializing the point variables
		int p1Points = 0;
		int p2Points = 0;
		//deck creation
		Deck warDeck = new Deck();
		//shuffling the deck
		warDeck.shuffle();
		//game loop, makes the 2 players draw and compare their cards, also prints the cards
		while(warDeck.deckLength() > 1){
			Card p1Card = warDeck.drawTopCard();
			Card p2Card = warDeck.drawTopCard();
			
			//card printing
			System.out.println(p1Card);
			System.out.println(p2Card);
			
			//card comparisons
			if(p1Card.calculateScore() > p2Card.calculateScore()){
				p1Points++;
			}
			if(p1Card.calculateScore() < p2Card.calculateScore()){
				p2Points++;
			}
			System.out.println("Player 1 points: " + p1Points + " // Player 2 points: " + p2Points);
		}
		//if statement that decides the winner or if it is a tie
		if(p1Points > p2Points){
			System.out.println("Congratulations on winning player 1!");
		}
		if(p1Points == p2Points){
			System.out.println("No one won, it's a tie");
		}else{
			System.out.println("Congratulations on winning player 2!");
		}
	}
}