import java.util.Random;

public class Deck{
	private Card[] cards;
	private int numberOfCards = 52;
	Random rng = new Random();
	public Deck(){
		this.cards = new Card[52];
		int[] values = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
		String suit = "card";
		int value = 0;
		//deck creator, this function sets the fields in the deck array
		for(int i = 0; i < cards.length; i++){
			if(i < 13  && i >= 0){
				value = values[i%13];
				suit = "Spades";
			}
			if(i < 26 && i >= 13){
				value = values[i%13];
				suit = "Hearts";
			}
			if(i < 39 && i >= 26){
				value = values[i%13];
				suit = "Clubs";
			}
			if(i < 52 && i >= 39){
				value = values[i%13];
				suit = "Diamonds";
			}
			cards[i] = new Card(suit, value);
		}
	}
	//deck length method, returns the length of the deck
		public int deckLength(){
			return numberOfCards;
		}
		//draw top card, draws the top card and returns it
		public Card drawTopCard(){
			this.numberOfCards--;
			return cards[numberOfCards];
		}
		//to string overwrite to properly print out the array
		public String toString(){
			String card = "";
			for(int i = 0; i < numberOfCards; i++){
				card = card + this.cards[i].toString()+ "\n";
			}
			return card;
		}
		//shuffle method, this method allows for the deck cards to change indexes
		public void shuffle(){
			
			for(int i = 0; i < numberOfCards; i++){
				int newPosition = rng.nextInt(numberOfCards);
				Card temp = this.cards[i];
				this.cards[i] = this.cards[newPosition];
				this.cards[newPosition] = temp;
			}
		}
}