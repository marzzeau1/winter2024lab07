public class Card{
	private String suit;
	private int value;
	//field setting
	public Card(String suit, int value){
		this.suit = suit;
		this.value = value;
	}
	//getter for suit
	public String getSuit(){
		return this.suit;
	}
	//getter for value
	public int getValue(){
		return this.value;
	}
	//toString overwrite so that the cards print out correclt
	public String toString(){
		return "Suit: " + this.suit + ", Value: " + this.value;
	}
	//This function calculates the score of a card
	public double calculateScore(){
		double cardScore = value;
		if(suit.equals("Hearts")){
			cardScore += 0.4;
		}
		if(suit.equals("Spades")){
			cardScore += 0.3;
		}
		if(getSuit().equals("Diamonds")){
			cardScore += 0.2;
		}
		else{
			cardScore += 0.1;
		}
		return cardScore;
	}
}